FROM openjdk:8-jdk-alpine3.9
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
RUN mkdir -p /apps/log
RUN chmod -R 777 /apps/log
ENTRYPOINT ["java","-jar","/app.jar"]