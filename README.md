# Naradacode - SpringBoot Example Backend Services
Example springboot backend service for DevSecOps Showcase
#Trigger Jenkins #39

## App Main Structure
* `src/main/java/com/naradacode/springbootexample/controller` - Controllers that are injected with the service interfaces
* `src/main/java/com/naradacode/springbootexample/service` - Services that are injected with the repository interfaces
* `src/main/java/com/naradacode/springbootexample/repository` - Springboot Data Repository interfaces
* `src/main/java/com/naradacode/springbootexample/entity` - Entities that map onto database tables

## App Test Structure
* `src/test/java/com/naradacode/springbootexample/controller` - Unit tests class for controller classes.
* `src/test/java/com/naradacode/springbootexample/service` - Unit tests class for service classes.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
- Java IDE: IntelliJ/Eclipse

### Installing
1.	Clone project 
	    ```
	    https://gitlab.com/naradacode/springboot-example.git
	    ```

2.	Build and package

        - Build and package using command: 
            ```
            mvnw clean package
            ```
    
        - Build and package using IDE:
        
            Open as maven project and run maven `clean package` goal on your preferred IDE

3.	Start springboot application:
    
        ```
        java -jar -Dspring.profiles.active=[sit/uat/prod] -Dserver.port=[server-port] [application-binary-file]
        ```
    
        For example:
        ```
        java -jar -Dspring.profiles.active=sit -Dserver.port=8181 target/springboot-example-0.0.1-SNAPSHOT.jar
        ```
      
4.	Start springboot application (on background process):
    
        ```
        nohup java -jar -Dspring.profiles.active=[sit/uat/prod] -Dserver.port=[server-port] [application-binary-file] > [log-file] &
        ```
    
        For example:
        ```
        nohup java -jar -Dspring.profiles.active=sit -Dserver.port=8181 target/springboot-example-0.0.1-SNAPSHOT.jar > app.log & 
        ```
      
        Stop spring boot application (on background process):
        ```
        kill [pid]
        ```
