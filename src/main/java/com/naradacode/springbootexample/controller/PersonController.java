package com.naradacode.springbootexample.controller;

import com.naradacode.springbootexample.entity.Person;
import com.naradacode.springbootexample.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {
    @Autowired
    PersonService personService;

    @Value("${feature.showunderdevelopment}")
    boolean flagUnderDevelopment;

    @GetMapping("/findbyfirstname")
    public List<Person> findByFirstName(@RequestParam(name = "firstname", required = true) String firstname){
        if(!flagUnderDevelopment)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        List<Person> personList = personService.findByFirstName(firstname);
        if (!personList.isEmpty())
            return personList;
        else
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Person not found");
    }

    @GetMapping("/teststacktrace")
    public Person testStackTrace(){
        Person person = new Person();
        throw new NullPointerException();
    }
}
