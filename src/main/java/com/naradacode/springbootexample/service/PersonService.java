package com.naradacode.springbootexample.service;

import com.naradacode.springbootexample.entity.Person;

import java.util.List;

public interface PersonService {
    List<Person> findByFirstName(String firstname);
}
