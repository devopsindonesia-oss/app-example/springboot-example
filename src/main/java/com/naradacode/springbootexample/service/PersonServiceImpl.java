package com.naradacode.springbootexample.service;

import ch.qos.logback.classic.Logger;
import com.naradacode.springbootexample.entity.Person;
import com.naradacode.springbootexample.repository.PersonRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {
    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(PersonServiceImpl.class);

    @Autowired
    PersonRepository personRepository;

    @Override
    public List<Person> findByFirstName(String firstname) {
        List<Person> result = personRepository.findByFirstName(firstname);
        LOGGER.debug("List<Person> result size: " + result.size());
        return result;
    }
}
