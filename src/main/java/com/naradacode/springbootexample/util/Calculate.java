package com.naradacode.springbootexample.util;

public class Calculate {
    int[] getDivide(int a, int b){
        if(b==0)
            return null;

        int quotient = a/b;
        int remainder = a % b;

        return new int[]{quotient, remainder};
    }

    int[] wrongDivide(int a, int b){
        int quotient = a/b;
        int remainder = a % b;

        return new int[]{quotient, remainder};
    }
}