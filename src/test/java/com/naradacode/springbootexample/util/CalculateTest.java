package com.naradacode.springbootexample.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class CalculateTest {
    @Test
    public void testGetDivide(){
        int[] values = new Calculate().getDivide(3,2);
        assertEquals(1, values[0]);
        assertEquals(1, values[1]);
    }

    @Test
    public void testGetDivide_ByZero(){
        int[] values = new Calculate().getDivide(1,0);
        assertNull(values);
    }
}
