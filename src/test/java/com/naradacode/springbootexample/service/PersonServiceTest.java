package com.naradacode.springbootexample.service;

import com.naradacode.springbootexample.entity.Person;
import com.naradacode.springbootexample.repository.PersonRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class PersonServiceTest {
    @Autowired
    private PersonService personService;

    @MockBean
    private PersonRepository personRepository;

    @Test
    void testGetPersonByName_Found() {
        //Create Mock for Repository Layer
        when(personRepository.findByFirstName("John")).thenReturn(
                Arrays.asList(new Person(1, "John", "Doe"))
        );

        //Call Service
        List<Person> personList = personService.findByFirstName("John");

        //Verify mock is called
        verify(personRepository).findByFirstName("John");

        //Verify result
        assertNotNull(personList);
        assertEquals("John", personList.get(0).getFirstName());
    }
}
