package com.naradacode.springbootexample.controller;

import com.naradacode.springbootexample.entity.Person;
import com.naradacode.springbootexample.service.PersonService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import java.util.Arrays;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@WebMvcTest(PersonController.class)
public class PersonControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PersonService personService;

    @Test
    public void testGetPerson_Found() throws Exception {
        //Create Mock Service Layer
        when(personService.findByFirstName("John")).thenReturn(
                Arrays.asList(new Person(1, "John", "Doe"))
        );

        //Request and get result
        ResultActions resultActions;
        resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/person/findbyfirstname?firstname=John").contentType(MediaType.APPLICATION_JSON));

        //Verify mock is called
        verify(personService).findByFirstName("John");

        //Verify result
        resultActions.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].firstName").value("John"));
    }

    @Test
    public void testGetPerson_NotFound() throws Exception {
        Person[] emptyArrPerson={};
        //Create Mock Service Layer
        when(personService.findByFirstName("John")).thenReturn(
                Arrays.asList(emptyArrPerson)
        );

        //Request and get result
        ResultActions resultActions;
        resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/person/findbyfirstname?firstname=John").contentType(MediaType.APPLICATION_JSON));

        //Verify mock is called
        verify(personService).findByFirstName("John");

        //Verify result
        resultActions.andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
